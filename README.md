# vue-sprite-2d

> Vue.js로 구현된 Sprite 2D 예제입니다.

<div align="center">
  <img src="./result.gif">
</div>

## 빌드 설정
``` bash
# 의존성 모듈 설치
npm i

# localhost:8080 핫 로드
npm run dev

# 빌드
npm run build
```
